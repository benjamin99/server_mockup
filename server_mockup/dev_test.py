__author__ = 'admin'

from proxy.views import ProxyView as View

v = View()
json = v._get_clean_json_from_string('''
    {
        "name": "Alice",            // [string]
        "age": 23,                  // [number]
        "url": "https://test.url",  // [string(url)]
        "info" : ["a", "b", "c"],   // [optional]
        "options": [                // [optional]
            "cat",
            "apple",
            ...
        ]
    }
''')

print(json)


import json

result = json.loads('''{
    "labReport" : [
            {
                "reportDate" : 1406938026,
                "content" : [
                    {
                        "item" : "RBC",
                        "labValue" : {
                            "value" :"2.66",
                            "status" : "normal"
                        }
                    }
                ]
            }
    ]
}''')

print(json.dumps(result))
