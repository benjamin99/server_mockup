from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.http.response import HttpResponse
from proxy.views import ProxyView
from web.views import LandingView


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'server_mockup.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^proxy/', ProxyView.as_view()),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', LandingView.as_view())
)
