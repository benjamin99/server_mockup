from django.http import HttpResponse
from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt
from server_mockup import settings
from urllib.request import urlopen, Request
from urllib.parse import urlencode
import json
import re

_comment    = re.compile('(^)?[^\S\n]*/(?:\*(.*?)\*/[^\S\n]*|/[^\n]*)($)?', re.DOTALL | re.MULTILINE)
_ellipsis   = re.compile(',[ \t\r\n]*\.{3}')
_url_prefix = re.compile('http[s]*:\/\/')

# Create your views here.
class ProxyView(View):
    _base_url = settings.BASE_URL

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(ProxyView, self).dispatch(request, args, kwargs)

    def get(self, request, *args, **kwargs):
        return self._handling_reqeust(request)

    def put(self, request, *args, **kwargs):
        return self._handling_reqeust(request)

    def post(self, request, *args, **kwargs):
        return self._handling_reqeust(request)

    def delete(self, request, *args, **kwargs):
        return self._handling_reqeust(request)

    def _handling_reqeust(self, request):
        json_object = None
        response, exception = self._forwarding_request(request)
        if response:
            content_type = response.headers.get('Content-Type', '')
            if not re.match(r'^application\/json$', content_type.lower()):
                raise NotImplementedError
            else:
                json_object, exception = self._get_clean_json_from_string(response.read().decode('utf-8'))

            if json_object:
                content = json.dumps(json_object)
                content_type = 'application/json'
                status = 200
            else:
                content = '{"error": { "message": "%s" }}' % str(exception)
                content_type = 'application/json'
                status = 500
        else:
            content = '{"error": { "message": "%s"}}' % str(exception)
            content_type = 'application/json'
            # TODO: get the proper exception
            status = 400

        return HttpResponse(content,
                            content_type=content_type,
                            status=status)

    # Request Handling Related: ----------------------------------------------------------------------------------------
    def _forwarding_request(self, request):
        response, exception = None, None
        fwd_reqeust = self._create_forwarding_request(request)
        try:
            response = urlopen(fwd_reqeust)
        except Exception as e:
            exception = e

        return response, exception

    def _get_forwarding_url_from_request_path(self, request_path):
        if settings.SHOULD_REMOVE_PREFIX:
            request_path = request_path[re.search(r'\/[a-zA-Z\_\-\.]+\/', request_path).end()-1:]

        url = '%s%s'% (self._base_url, request_path)
        if not re.match(r'^http[s]*://.+', url):
            url = 'https://%s' % url
        return url

    def _create_forwarding_request(self, request):
        full_url = self._get_forwarding_url_from_request_path(request.path)

        # Handle the request headers/body:
        if request.method.lower() == 'post':
            data = urlencode(request.POST.dict()).encode('utf-8')
            headers = self._retrive_clean_headers(request)
        elif request.method.lower() == 'put':
            data = request.read()
            headers = self._retrive_clean_headers(request)
        else:
            data = None
            headers = {}

        fwd_requeset = Request(full_url, data, headers)
        if request.method.lower() == 'delete' or request.method.lower() == 'put':
            fwd_requeset.method = request.method.upper()

        return fwd_requeset

    def _retrive_clean_headers(self, request):
        regex   = re.compile('^HTTP_')
        headers = dict((regex.sub('', header), value)
                    for (header, value) in request.META.items() if header.startswith('HTTP_'))
        del headers['HOST']

        return headers


    # JSON process related: --------------------------------------------------------------------------------------------
    def _get_clean_json_from_string(self, json_string):
        print("TEST: \n %s" % json_string)
        json_object, exception = None, None
        json_string = ProxyView._remove_comment_from_string(json_string)
        json_string = ProxyView._remove_ellipsis_from_string(json_string)

        print("clean json: \n%s" % json_string)
        # print("error @: \n" % json_string[1940:1950])
        try:
            json_object = json.loads(json_string)
        except Exception as e:
            exception = e
        return json_object, exception

    def _remove_comment_from_string(json_string):
        found_matches = list()
        index_shift   = 0
        match = _comment.search(json_string[index_shift:])
        while match:
            url_prefix_match = _url_prefix.search(json_string[index_shift:])
            if url_prefix_match:
                if url_prefix_match.end() == match.start()+2:
                    index_shift = index_shift + url_prefix_match.end()
                    match = _comment.search(json_string[index_shift:])
                    continue

            found_matches.append([match.start()+index_shift, match.end()+index_shift])
            index_shift += match.end()
            match = _comment.search(json_string[index_shift:])

        content = json_string
        index_shift = 0
        for span in found_matches:
            content = content[:span[0]-index_shift] + content[span[1]-index_shift:]
            index_shift += (span[1] - span[0])

        return content

    def _remove_ellipsis_from_string(json_string):
        content = json_string
        match   = _ellipsis.search(json_string)
        while match:
            content = content[:match.start()] + content[match.end():]
            match   = _ellipsis.search(content)
        return content