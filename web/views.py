from django.http.response import HttpResponse
from django.views.generic import View
from server_mockup import settings


class LandingView(View):
    def get(self, request, *args, **kwargs):
        return HttpResponse(
            '''<http>
                <h1>Wantoto Mockup Server</h1>
                <a>It Works!</a>
                <p><b>API content source:</b> %s</p>
            </http>''' % settings.BASE_URL)