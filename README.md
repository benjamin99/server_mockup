# MOCKUP SERVER for ApiAry

## Usage

1. Setup **BASE_URL** env: 
			
			echo BASE_URL=[THE APIARY MOCK SERVER]

2. Run the django server with localhost:

			python manage.py runserver 3000
			
3. Testing corresponding APIs with prefix: **/proxy/**

			example: http://localhost:3000/proxy/sessionKey 